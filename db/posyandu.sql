-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 06, 2020 at 03:12 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `posyandu`
--

-- --------------------------------------------------------

--
-- Table structure for table `balita`
--

CREATE TABLE `balita` (
  `id_balita` int(11) NOT NULL,
  `nik_ortu` int(11) NOT NULL,
  `nama_bayi` varchar(255) NOT NULL,
  `tempatlhr_bayi` varchar(255) NOT NULL,
  `tanggallhr_bayi` date NOT NULL,
  `nama_ortu` varchar(255) NOT NULL,
  `alamat_ortu` text NOT NULL,
  `jenis_kelamin` varchar(255) NOT NULL,
  `usia_bayi` int(11) NOT NULL,
  `berat_lahir` decimal(10,0) NOT NULL,
  `panjang_lahir` decimal(10,0) NOT NULL,
  `goldar_bayi` varchar(255) NOT NULL,
  `berat_sekarang` decimal(10,0) NOT NULL,
  `panjang_sekarang` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id_barang` int(15) NOT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `pemilik_barang` varchar(50) NOT NULL,
  `jenis_barang` varchar(50) NOT NULL,
  `gambar_barang` varchar(255) NOT NULL,
  `harga_barang` int(25) NOT NULL,
  `batas_tebus_barang` date NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_barang`, `nama_barang`, `pemilik_barang`, `jenis_barang`, `gambar_barang`, `harga_barang`, `batas_tebus_barang`, `status`) VALUES
(1, 'Xiaomi Redmi Note 10', 'Atep', 'Handphone', '1593173255redminote9.jpg', 2000000, '2020-06-24', 1),
(2, 'Samsung Galaxy', 'Tantono', 'Handphone', '1593173269zflip.jpg', 2400000, '2020-06-24', 1),
(3, 'Honda Beat 2020', 'Dadang', 'Motor', '1593180167beat.png', 12000000, '2020-08-08', 1),
(4, 'Toyota Avanza 1.5 S', 'Jujun', 'Mobil', '1593196428avanza.jpg', 105000000, '2020-10-10', 1),
(5, 'Asus Vivobook A412', 'David', 'Laptop', '1593632067asus-vivobook-a412.jpg', 6750000, '2020-06-05', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ibu_hamil`
--

CREATE TABLE `ibu_hamil` (
  `id_ibuhamil` int(5) NOT NULL,
  `nik_ibuhamil` int(255) NOT NULL,
  `nama_ibuhamil` varchar(255) NOT NULL,
  `umur` int(255) NOT NULL,
  `tempatlhr_ibuhamil` varchar(255) NOT NULL,
  `tanggallhr_ibuhamil` date NOT NULL,
  `tinggi` decimal(10,0) NOT NULL,
  `berat badan` int(255) NOT NULL,
  `usiakandungan` int(11) NOT NULL,
  `kandunganke` int(11) NOT NULL,
  `hpht` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(15) NOT NULL,
  `nama_kategori` varchar(50) NOT NULL,
  `jumlah_barang` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`, `jumlah_barang`) VALUES
(1, 'Handphone', 0),
(2, 'Laptop', 0),
(3, 'Motor', 0),
(4, 'Mobil', 0);

-- --------------------------------------------------------

--
-- Table structure for table `lelang`
--

CREATE TABLE `lelang` (
  `id_lelang` int(11) NOT NULL,
  `id_barang_lelang` int(11) NOT NULL,
  `id_penawar` int(11) NOT NULL,
  `harga_awal` varchar(50) NOT NULL,
  `harga_pemenang` varchar(50) NOT NULL,
  `tanggal_lelang` date NOT NULL,
  `status_lelang` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `penawaran_lelang`
--

CREATE TABLE `penawaran_lelang` (
  `id_penawar_lelang` int(11) NOT NULL,
  `id_barang_lelang` int(11) NOT NULL,
  `id_penawar` int(11) NOT NULL,
  `nama_penawar` varchar(50) NOT NULL,
  `harga_penawaran` varchar(50) NOT NULL,
  `status_penawaran` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penawaran_lelang`
--

INSERT INTO `penawaran_lelang` (`id_penawar_lelang`, `id_barang_lelang`, `id_penawar`, `nama_penawar`, `harga_penawaran`, `status_penawaran`) VALUES
(1, 1, 2, 'Joko', '2050000', 'Pending'),
(2, 2, 2, 'Nana', '2450000', 'Pending'),
(3, 3, 2, 'Kiki', '12050000', 'Pending'),
(4, 4, 2, 'Budi', '110000000', 'Pending'),
(5, 2, 3, 'Diki', '2500000', 'Pending');

-- --------------------------------------------------------

--
-- Table structure for table `penimbangan_bayi`
--

CREATE TABLE `penimbangan_bayi` (
  `id_penimbangan` int(11) NOT NULL,
  `nik_ortu` int(11) NOT NULL,
  `tanggal_timbang` date NOT NULL,
  `usia` int(11) NOT NULL,
  `berat_badan` decimal(11,0) NOT NULL,
  `lingkar_perut` decimal(11,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `penimbangan_ibuhamil`
--

CREATE TABLE `penimbangan_ibuhamil` (
  `id_penimbangan` int(11) NOT NULL,
  `nik_ibuhamil` int(11) NOT NULL,
  `tanggal_timbang` date NOT NULL,
  `usia` int(11) NOT NULL,
  `berat_badan` decimal(11,0) NOT NULL,
  `lingkar_perut` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(20) NOT NULL,
  `nik` int(255) NOT NULL,
  `username` varchar(100) NOT NULL,
  `nama_lengkap` text NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(150) NOT NULL,
  `level` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `nik`, `username`, `nama_lengkap`, `email`, `password`, `level`) VALUES
(1, 0, 'admin', 'Miftahul Ulum', 'admin@localhost', '21232f297a57a5a743894a0e4a801fc3', 1),
(2, 0, 'mfthlun', 'Miftahul Ulum', 'mfthlun@gmail.com', '7e7cfc6b96315dbe34a823be9a451f83', 2),
(9, 0, 'cad', 'Risyad', 'cad@gmail.com', 'b5fde512c76571c8afd6a6089eaaf42a', 2),
(17, 0, 'cok', 'cok', 'cok@gmail.com', '595aa739fc58403c7c62cc2840d0b7fb', 2),
(18, 0, 'wkwk', 'wkwk', 'wkwk@', 'ede9b60d7f83a4cfd3fb3e9ceda2a0e1', 2),
(19, 0, 'wkwk', 'wkwk', 'wkwk', '0bef1939b3c02eea4b89f1a8247419cf', 2),
(20, 0, 'cpk', 'cpl', 'cpl', 'b09197d6f45a6402d5d824ef9ebea0f0', 2),
(21, 0, 'cok', 'cok', 'cok', '595aa739fc58403c7c62cc2840d0b7fb', 2),
(22, 0, 'cocote', 'mate', 'wkwkwk', 'ede9b60d7f83a4cfd3fb3e9ceda2a0e1', 2),
(23, 0, 'ehehehe', 'maap ', 'ya', '0079fcb602361af76c4fd616d60f9414', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `ibu_hamil`
--
ALTER TABLE `ibu_hamil`
  ADD PRIMARY KEY (`nik_ibuhamil`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `lelang`
--
ALTER TABLE `lelang`
  ADD PRIMARY KEY (`id_lelang`);

--
-- Indexes for table `penawaran_lelang`
--
ALTER TABLE `penawaran_lelang`
  ADD PRIMARY KEY (`id_penawar_lelang`);

--
-- Indexes for table `penimbangan_bayi`
--
ALTER TABLE `penimbangan_bayi`
  ADD PRIMARY KEY (`nik_ortu`);

--
-- Indexes for table `penimbangan_ibuhamil`
--
ALTER TABLE `penimbangan_ibuhamil`
  ADD PRIMARY KEY (`nik_ibuhamil`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id_barang` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `lelang`
--
ALTER TABLE `lelang`
  MODIFY `id_lelang` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `penawaran_lelang`
--
ALTER TABLE `penawaran_lelang`
  MODIFY `id_penawar_lelang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
