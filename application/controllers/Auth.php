<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller{
  function __construct(){
    parent::__construct();
    $this->load->model('Auth_m');
  }

  function index(){
    $this->login();
  }

  // Login, Register, Logout

  function login(){
    if ($this->session->userdata('email')&&$this->session->userdata('logged_in')){
      redirect_role($this->session->userdata('level'));
    }else{
      $this->load->view('login_view');
    }
  }

  function login_auth(){
    $email    = $this->input->post('email',TRUE);
    $password = md5($this->input->post('password',TRUE));
    $validate = $this->Auth_m->validate_login($email,$password);
    if($validate->num_rows() > 0){
      $data  = $validate->row_array();
      $user_id = $data['user_id'];
      $username = $data['username'];
      $email = $data['email'];
      $level = $data['level'];
      $data_session = array(
        'user_id' => $user_id,
        'username' => $username,
        'email'     => $email,
        'level'     => $level,
        'logged_in' => TRUE
      );
      $this->session->set_userdata($data_session);
      redirect_role($level);
    }else{
      echo $this->session->set_flashdata('msg','Email atau Password salah!');
      redirect('/');
    }
  }

  function register(){
    if ($this->session->userdata('email')&&$this->session->userdata('logged_in')){
      redirect($this->session->userdata('level'));
    }else{
      $this->load->view('register_view');
    }
  }

  function register_auth(){
    $username = $this->input->post('username',TRUE);
    $nama_lengkap = $this->input->post('nama_lengkap',TRUE);
    $email    = $this->input->post('email',TRUE);
    $password = md5($this->input->post('password',TRUE));
    $validate = $this->Auth_m->validate_register($email);
    if($validate->num_rows() > 0){
      echo $this->session->set_flashdata('msg','Email sudah terdaftar! Silahkan login');
      redirect('/admin/user_input');
    }else{
      $data_register = array(
        'username' => $username,
        'nama_lengkap' => $nama_lengkap,
        'email' => $email,
        'password' => $password,
        'level' => 2
      );
      $this->Auth_m->register_role($data_register);
      echo $this->session->set_flashdata('msg_success','Silahkan login');
      redirect('/admin/user_input');
    }

  }

  function logout(){
      $this->session->sess_destroy();
      redirect('/');
  }

}