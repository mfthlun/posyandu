<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller{
  function __construct(){
    parent::__construct();
    login_status();
    $this->load->model(array('Barang_m','Lelang_m','Kategori_m'));
  }

  function index(){
    //Allow user/pelelang access
    if($this->session->userdata('level')==='2'){
      $data['title'] = "Aplikasi Lelang Online";
      $this->template->load('user/_template', 'user/dashboard', $data);
    }else{
      echo "Access Denied";
    }
  }
  
  // ------------------------
  // --CRUD JELAJAH LELANG---
  // ------------------------
  
  function browse(){
    $data['title'] = "Jelajahi Lelang";
    $data['data_barang'] = $this->Barang_m->SelectStatusAktif();
    $this->template->load('user/_template', 'user/lelang/view',$data);
  }

  function detail_lelang(){
    $data['title'] = "Lelang barang";
    $id_barang = $this->uri->segment(3);
    if (!isset($id_barang)) redirect('user/browse');
    $data['data_barang'] = $this->Barang_m->SelectByID($id_barang);
    if (!$data["data_barang"]) show_404();
    $data['data_tawar'] = $this->Lelang_m->SelectPenawaran($id_barang);
    $this->template->load('user/_template', 'user/lelang/detail',$data);
  }

  function tawar_action() {
    $id_barang_lelang = $this->input->post('id_barang_lelang');
    $id_penawar = $this->input->post('id_penawar');
    $nama_penawar = $this->input->post('nama_penawar');
    $harga_penawaran = $this->input->post('harga_penawaran');
    $status_penawaran = $this->input->post('status_penawaran');
    $data = array(
      'id_barang_lelang' => $id_barang_lelang,
      'id_penawar' => $id_penawar,
      'nama_penawar' => $nama_penawar,
      'harga_penawaran' => $harga_penawaran,
      'status_penawaran' => $status_penawaran,
    );

    $harga_barang = $this->Barang_m->SelectHargaBarangById($id_barang_lelang);
    $harga_penawaran_max = $this->Lelang_m->SelectHargaPenawaran($id_barang_lelang);
    if(isset($harga_penawaran_max->harga_penawaran)){
      $harga_lelang = $harga_penawaran_max->harga_penawaran;
    }else{
      $harga_lelang = $harga_barang->harga_barang;
    }

    if($harga_penawaran > $harga_lelang) {
      if($this->Lelang_m->Insert($data)){
        //set flashdata belum muncul
        $this->session->set_flashdata('berhasil', 'Data berhasil Disimpan');
        redirect(site_url('user/detail_lelang/'.$id_barang_lelang));
      } else { 
        //set flashdata belum muncul
        $this->session->set_flashdata('gagal', 'Data Gagal Disimpan, Coba Lagi');
        redirect(site_url('user/detail_lelang/'.$id_barang_lelang));
      }
    }else {
      $this->session->set_flashdata('berhasil', 'Data berhasil Disimpan');
      redirect(site_url('user/detail_lelang/'.$id_barang_lelang));
    }
  }
  
  // ------------------------
  // --CRUD KATEGORI LELANG--
  // ------------------------
  
  function kategori(){
    $data['title'] = "Kategori Lelang";
    $data['data_kategori'] = $this->Kategori_m->SelectAll();
    $data['data_barang'] = $this->Barang_m->SelectStatusAktif();
    $this->template->load('user/_template', 'user/kategori/view',$data);
  }
  
  // ------------------------
  // ------CRUD PROFIL-------
  // ------------------------
  
  function profil(){
    $data['title'] = "Profil Saya";
    // $data['data_user'] = $this->User_m->SelectAll();
    $this->template->load('user/_template', 'user/profil/view',$data);
  }
  
  // ------------------------
  // ------CRUD RIWAYAT------
  // ------------------------
  
  function riwayat(){
    $data['title'] = "History Lelang";
    $id_user = $this->session->userdata('user_id');
    $data['data_barang'] = $this->Barang_m->SelectAll();
    $data['data_tawar'] = $this->Lelang_m->SelectPenawaranByUser($id_user);
    $this->template->load('user/_template', 'user/history/view',$data);
  }
}