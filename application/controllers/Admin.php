<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller{
  function __construct(){
    parent::__construct();
    login_status();
    $this->load->model(array('Barang_m','User_m','Kategori_m'));
    // $this->load->library('form_validation');
  }

  function index(){
    //Allow admin access
    if($this->session->userdata('level')==='1'){
      $data['title'] = "Aplikasi Lelang Online";
      $this->template->load('admin/_template2', 'admin/dashboard', $data);
    }else{
      echo "Access Denied";
    }
  }

  // ------------------------
  // ------CRUD BARANG-------
  // ------------------------

  function barang(){
    $data['title'] = "Daftar Barang Lelang";
    $data['data_barang'] = $this->Barang_m->SelectAll();
    $this->template->load('admin/_template', 'admin/barang/view',$data);
  }

  function barang_detail(){
		$id = $this->uri->segment(3);
    if (!isset($id)) redirect('admin/barang');
    $data['title'] = "Detail Barang Lelang";
    $data['data_barang'] = $this->Barang_m->SelectByID($id);
    if (!$data["data_barang"]) show_404();
    $this->template->load('admin/_template', 'admin/barang/detail',$data);
  }

  function barang_input() {
    $data['title'] = "Tambah Barang Lelang";
    $data['kategori'] = $this->Kategori_m->SelectAll();
    $this->template->load('admin/_template', 'admin/barang/input',$data);
  }

  function barang_input_action() {
    $pemilik_barang = $this->input->post('pemilik_barang');
    $nama_barang = $this->input->post('nama_barang');
    $jenis_barang = $this->input->post('jenis_barang');
    $harga_barang = $this->input->post('harga_barang');
    $batas_tebus_barang = $this->input->post('batas_tebus_barang');
    
    $new_name                       = time().$_FILES["gambar_barang"]['name'];
    $config['upload_path']          = './upload/lelang/';
    $config['allowed_types']        = 'gif|jpg|png';
    $config['file_name']            = $new_name;
    $config['remove_spaces']        = TRUE;
    $config['overwrite']			      = TRUE;
    // $config['max_size']             = 1024; // 1MB

    $this->load->library('upload', $config);

    if ($this->upload->do_upload('gambar_barang')) {
      $gambar_barang = $this->upload->data("file_name");
    }

    $data = array(
      'pemilik_barang' => $pemilik_barang,
      'nama_barang' => $nama_barang,
      'jenis_barang' => $jenis_barang,
      'gambar_barang' => $gambar_barang,
      'harga_barang' => $harga_barang,
      'batas_tebus_barang' => $batas_tebus_barang
    );

    if($this->Barang_m->Insert($data)){
      //set flashdata belum muncul
      $this->session->set_flashdata('success', 'Berhasil disimpan');
      redirect(site_url('Admin/barang'));
    } else { 
      //set flashdata belum muncul
      $this->session->set_flashdata('gagal', 'Data Gagal Disimpan, Coba Lagi');
      redirect(site_url('Admin/barang_input'));
    }
  }

  function barang_edit() {
		$id = $this->uri->segment(3);
    if (!isset($id)) redirect('admin/barang');
    $data['title'] = "Ubah Barang Lelang";
    $data['data_barang'] = $this->Barang_m->SelectByID($id);
    $data['kategori'] = $this->Kategori_m->SelectAll();
    if (!$data["data_barang"]) show_404();
    $this->template->load('admin/_template', 'admin/barang/edit',$data);
  }

  function barang_edit_action() {
    $id_barang = $this->input->post('id_barang');
    $pemilik_barang = $this->input->post('pemilik_barang');
    $nama_barang = $this->input->post('nama_barang');
    $jenis_barang = $this->input->post('jenis_barang');
    $harga_barang = $this->input->post('harga_barang');
    $batas_tebus_barang = $this->input->post('batas_tebus_barang');
    $gambar_barang_old = $this->input->post('gambar_barang_old');
    $status_lelang = $this->input->post('status_lelang');

    if(!empty($_FILES["gambar_barang"]['name'])){
      $new_name                       = time().$_FILES["gambar_barang"]['name'];
      $config['upload_path']          = './upload/lelang/';
      $config['allowed_types']        = 'gif|jpg|png';
      $config['file_name']            = $new_name;
      $config['remove_spaces']        = TRUE;
      $config['overwrite']			      = TRUE;

      $this->load->library('upload', $config);

      if ($this->upload->do_upload('gambar_barang')) {
        unlink("upload/lelang/".$gambar_barang_old);
        $gambar_barang = $this->upload->data("file_name");
      }

      $data = array(
        'id_barang' => $id_barang,
        'pemilik_barang' => $pemilik_barang,
        'nama_barang' => $nama_barang,
        'jenis_barang' => $jenis_barang,
        'gambar_barang' => $gambar_barang,
        'harga_barang' => $harga_barang,
        'batas_tebus_barang' => $batas_tebus_barang,
        'status' => $status_lelang
      );
    }else{
      $data = array(
        'id_barang' => $id_barang,
        'pemilik_barang' => $pemilik_barang,
        'nama_barang' => $nama_barang,
        'jenis_barang' => $jenis_barang,
        'harga_barang' => $harga_barang,
        'batas_tebus_barang' => $batas_tebus_barang,
        'status' => $status_lelang
      );
    }

    if($this->Barang_m->Update($data)){
      //set flashdata belum muncul
      $this->session->set_flashdata('success', 'Berhasil diupdate');
      redirect(site_url('Admin/barang'));
    } else { 
      //set flashdata belum muncul
      $this->session->set_flashdata('gagal', 'Data Gagal diupdate, Coba Lagi');
      redirect(site_url('Admin/barang'));
    }
  }

  function barang_hapus()
	{
    $data['id_barang'] = $this->uri->segment(3);
    if (!isset($data['id_barang'])) redirect('admin/barang');
    if($this->Barang_m->Delete($data)){
      //set flashdata belum muncul
      $this->session->set_flashdata('success', 'Berhasil dihapus');
      redirect(site_url('Admin/barang'));
    } else { 
      //set flashdata belum muncul
      $this->session->set_flashdata('gagal', 'Data Gagal dihapus, Coba Lagi');
      redirect(site_url('Admin/barang'));
    }
	}

  // ------------------------
  // -------CRUD USER--------
  // ------------------------

  function user(){
    $data['title'] = "Daftar Pengguna";
    $data['data_user'] = $this->User_m->SelectAll();
    $this->template->load('admin/_template2', 'admin/user/view',$data);
  }

  function user_input() {
    $data['title'] = "Tambah Pengguna";
    $this->template->load('admin/_template2', 'admin/user/input',$data);
  }
  
  function user_input_action() {
    $username = $this->input->post('username');
    $nama_lengkap = $this->input->post('nama_lengkap');
    $password = $this->input->post('password');
    $level = $this->input->post('level');
    $email = $this->input->post('email');
    
    $data = array(
      'username' => $username,
      'password' => $password,
      'level' => $level,
      'email' => $email,
      'level' => 2
    );
    
    if($this->User_m->Insert($data)){
      //set flashdata belum muncul
      // $this->session->set_flashdata('success', 'Berhasil disimpan');
      $this->session->set_flashdata('success','User Berhasil Ditambah');
      redirect(site_url('Admin/user_input'));
    } else { 
      //set flashdata belum muncul
      $this->session->set_flashdata('gagal', 'Data Gagal Disimpan, Coba Lagi');
      redirect(site_url('Admin/user_input'));
    }
  }

  function user_edit() {
		$id = $this->uri->segment(3);
    if (!isset($id)) redirect('admin/user');
    $data['title'] = "Ubah Pengguna";
    $data['data_user'] = $this->User_m->SelectByID($id);
    if (!$data["data_user"]) show_404();
    $this->template->load('admin/_template', 'admin/user/edit',$data);
  }

  function user_edit_action() {
    $user_id = $this->input->post('user_id');
    $username = $this->input->post('username');
    $nama_lengkap = $this->input->post('nama_lengkap');
    $password = md5($this->input->post('password'));
    $level = $this->input->post('level');
    $email = $this->input->post('email');

    $data = array(
      'user_id' => $user_id,
      'username' => $username,
      'nama_lengkap' => $nama_lengkap,
      'password' => $password,
      'level' => $level,
      'email' => $email,
      'level' => 2
    );

    if($this->User_m->Update($data)){
      //set flashdata belum muncul
      $this->session->set_flashdata('success', 'Berhasil diupdate');
      redirect(site_url('Admin/user'));
    } else { 
      //set flashdata belum muncul
      $this->session->set_flashdata('gagal', 'Data Gagal diupdate, Coba Lagi');
      redirect(site_url('Admin/user'));
    }
  }

  function user_hapus()
	{
    $data['user_id'] = $this->uri->segment(3);
    if (!isset($data['user_id'])) redirect('admin/user');
    if($this->User_m->Delete($data)){
      //set flashdata belum muncul
      $this->session->set_flashdata('success', 'Berhasil dihapus');
      redirect(site_url('Admin/user'));
    } else { 
      //set flashdata belum muncul
      $this->session->set_flashdata('gagal', 'Data Gagal dihapus, Coba Lagi');
      redirect(site_url('Admin/user'));
    }
  }

  function user_detail(){
		$id = $this->uri->segment(3);
    if (!isset($id)) redirect('admin/user');
    $data['title'] = "Detail Data User";
    $data['data_user'] = $this->User_m->SelectByID($id);
    if (!$data["data_user"]) show_404();
    $this->template->load('admin/_template', 'admin/user/detail',$data);
  }

  // ------------------------
  // -----CRUD KATEGORI------
  // ------------------------

  function kategori(){
    $data['title'] = "Kategori Barang";
    $data['data_kategori'] = $this->Kategori_m->SelectAll();
    $this->template->load('admin/_template', 'admin/kategori/view',$data);
  }

  function kategori_input() {
    $data['title'] = "Tambah Kategori";
    $this->template->load('admin/_template', 'admin/kategori/input',$data);
  }

  function kategori_input_action() {
    $nama_kategori = $this->input->post('nama_kategori');

    $data = array(
      'nama_kategori' => $nama_kategori
    );

    if($this->Kategori_m->Insert($data)){
      //set flashdata belum muncul
      $this->session->set_flashdata('success', 'Berhasil disimpan');
      redirect(site_url('Admin/kategori'));
    } else { 
      //set flashdata belum muncul
      $this->session->set_flashdata('gagal', 'Data Gagal Disimpan, Coba Lagi');
      redirect(site_url('Admin/kategori'));
    }
  }

  function kategori_edit() {
		$id = $this->uri->segment(3);
    if (!isset($id)) redirect('admin/kategori');
    $data['title'] = "Ubah Kategori";
    $data['data_kategori'] = $this->Kategori_m->SelectByID($id);
    if (!$data["data_kategori"]) show_404();
    $this->template->load('admin/_template', 'admin/kategori/edit',$data);
  }

  function kategori_edit_action() {
    $id_kategori = $this->input->post('id_kategori');
    $nama_kategori = $this->input->post('nama_kategori');

    $data = array(
      'id_kategori' => $id_kategori,
      'nama_kategori' => $nama_kategori
    );

    if($this->Kategori_m->Update($data)){
      //set flashdata belum muncul
      $this->session->set_flashdata('success', 'Berhasil diupdate');
      redirect(site_url('Admin/kategori'));
    } else { 
      //set flashdata belum muncul
      $this->session->set_flashdata('gagal', 'Data Gagal diupdate, Coba Lagi');
      redirect(site_url('Admin/kategori'));
    }
  }

  function kategori_hapus()
	{
    $data['id_kategori'] = $this->uri->segment(3);
    if (!isset($data['id_kategori'])) redirect('admin/kategori');
    if($this->Kategori_m->Delete($data)){
      //set flashdata belum muncul
      $this->session->set_flashdata('success', 'Berhasil dihapus');
      redirect(site_url('Admin/kategori'));
    } else { 
      //set flashdata belum muncul
      $this->session->set_flashdata('gagal', 'Data Gagal dihapus, Coba Lagi');
      redirect(site_url('Admin/kategori'));
    }
  }

  // ------------------------
  // ------CRUD LELANG-------
  // ------------------------

  function lelang(){
    $data['title'] = "Daftar Lelang";
    $data['data_user'] = $this->User_m->SelectAll();
    $this->template->load('admin/_template', 'admin/lelang/view',$data);
  }
  
  // ------------------------
  // ----CRUD PENAWARAN------
  // ------------------------

  function penawaran(){
    $data['title'] = "Penawaran Lelang";
    $data['data_user'] = $this->User_m->SelectAll();
    $this->template->load('admin/_template', 'admin/penawaran/view',$data);
  }
  
  // ------------------------
  // ------CRUD LAPORAN------
  // ------------------------
  
  function laporan(){
    $data['title'] = "Laporan Lelang";
    $data['data_user'] = $this->User_m->SelectAll();
    $this->template->load('admin/_template', 'admin/laporan/view',$data);
  }
  
}