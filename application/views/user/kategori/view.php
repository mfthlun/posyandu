<!-- Header -->
<div class="header bg-success pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Kategori Lelang</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home text-success"></i></a></li>
                  <li class="breadcrumb-item active" aria-current="page">Kategori Lelang</li>
                </ol>
              </nav>
            </div>
            <!-- <div class="col-lg-6 col-5 text-right">
              <a href="#" class="btn btn-sm btn-neutral text-success">New</a>
              <a href="#" class="btn btn-sm btn-neutral text-success">Filters</a>
            </div> -->
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <?php
        $no = 1;
        foreach($data_kategori as $data_):
      ?>
      <div class="row justify-content-center">
        <div class="col">
          <div class="card">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 class="mb-0"><?php echo $data_->nama_kategori ?></h3>
                </div>
                <div class="col text-right">
                  <a href="#!" class="btn btn-sm btn-success">See all</a>
                </div>
              </div>
            </div>
            <div class="card-body">
              <div class="row">
                <?php
                  $no = 1;
                  foreach($data_barang as $data):
                    if($data->jenis_barang==$data_->nama_kategori){
                ?>
                <div class="col-lg-3 col-md-6">
                  <div class="card">
                    <img class="card-img-top" style="width: 100%; height: 200px; object-fit: cover;" src="<?php echo base_url() ?>/upload/lelang/<?php echo $data->gambar_barang ?>" alt="Card image cap">
                    <div class="card-body">
                      <h4 class="card-title mb-2"><?php echo $data->nama_barang ?></h4>
                      <h5 class="card-subtitle mb-2"><?php echo $data->harga_barang ?></h5>
                      <!-- <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p> -->
                      <a href="<?php echo base_url() ?>user/detail_lelang/<?php echo $data->id_barang ?>" class="btn-sm text-center btn-info btn-block">Detail</a>
                      <!-- <a href="#" class="btn-sm btn-success">Lelang</a> -->
                    </div>
                  </div>
                </div>
                <?php 
                    }
                  endforeach
                ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php 
        endforeach
      ?>
      <!-- Footer -->
      <footer class="footer pt-0">
        <div class="row align-items-center justify-content-lg-between">
          <div class="col-lg-6">
            <div class="copyright text-center  text-lg-left  text-muted">
              &copy; 2020 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1 text-success" target="_blank">Creative Tim</a>
            </div>
          </div>
          <div class="col-lg-6">
            <ul class="nav nav-footer justify-content-center justify-content-lg-end">
              <li class="nav-item">
                <a href="https://www.creative-tim.com" class="nav-link" target="_blank">Creative Tim</a>
              </li>
              <li class="nav-item">
                <a href="https://www.creative-tim.com/presentation" class="nav-link" target="_blank">About Us</a>
              </li>
              <li class="nav-item">
                <a href="http://blog.creative-tim.com" class="nav-link" target="_blank">Blog</a>
              </li>
              <li class="nav-item">
                <a href="https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md" class="nav-link" target="_blank">MIT License</a>
              </li>
            </ul>
          </div>
        </div>
      </footer>
    </div>