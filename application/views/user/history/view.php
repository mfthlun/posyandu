<!-- Header -->
<div class="header bg-success pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <!-- <h6 class="h2 text-white d-inline-block mb-0">Barang</h6> -->
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-home text-success"></i></a></li>
              <li class="breadcrumb-item active" aria-current="page">History Lelang</li>
            </ol>
          </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
          <!-- <a href="<?php echo base_url('Admin/barang_input') ?>" class="btn btn-neutral text-success">Input Barang</a> -->
          <!-- <a href="#" class="btn btn-neutral text-success">Filters</a> -->
        </div>
      </div>
      <!-- Card stats -->
      <div class="row">
        <div class="col-xl-3 col-md-6">
          
        </div>
      </div>
    </div>
  </div>
</div>
    
<!-- Page content - Tabel data -->

<div class="container-fluid mt--6">
  <div class="row">
    <div class="col-xl-12">
      <div class="card">
        <div class="card-header border-0">
          <div class="row align-items-center">
            <div class="col">
              <h3 class="mb-0">History Penawaran Lelang Saya</h3>
            </div>
          </div>
        </div>
        <div class="table-responsive">
          <!-- Tabel -->
          <table id="maintable" class="table align-items-center table-flush">
            <thead class="thead-light">
              <tr>
                <th scope="col">No.</th>
                <th scope="col">Nama Barang</th>
                <th scope="col">Pemilik</th>
                <th scope="col">Batas Penebusan</th>
                <th scope="col">Penawaran Saya</th>
                <th scope="col">Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $no = 1;
              if(!empty($data_tawar)){
                foreach($data_tawar as $data):
              ?>
              <tr>
                <td scope="col"><?= $no++; ?></td>
                <td scope="col"><?= $data_barang[$data->id_barang_lelang-1]->nama_barang; ?></td>
                <td scope="col"><?= $data_barang[$data->id_barang_lelang-1]->pemilik_barang; ?></td>
                <td scope="col"><?= $data_barang[$data->id_barang_lelang-1]->batas_tebus_barang; ?></td>
                <td scope="col"><?= $data->harga_penawaran; ?></td>
                <td scope="col">
                  <a href="<?= base_url() ?>user/detail_lelang/<?php echo $data_barang[$data->id_barang_lelang-1]->id_barang; ?>" class="btn btn-sm btn-success text-neutral">Detail</a>
                  <!-- <a href="#" class="btn btn-sm btn-success text-neutral">Lelang</a>
                  <a href="#" class="btn btn-sm btn-neutral text-success">Edit</a>
                  <a href="#" class="btn btn-sm btn-danger text-neutral" onclick="return confirm('Apakah anda yakin untuk menghapus?');">Hapus</a> -->
                </td>
              </tr>
              <?php 
                endforeach;
              }else{ ?>
              <tr>
                  <td colspan="6" class="text-center">Anda belum pernah melakukan penawaran lelang</td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>


  <!-- Footer -->
  <footer class="footer pt-0">
    <div class="row align-items-center justify-content-lg-between">
      <div class="col-lg-6">
        <div class="copyright text-center  text-lg-left  text-muted">
          &copy; 2020 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1 text-success" target="_blank">Creative Tim</a>
        </div>
      </div>
      <div class="col-lg-6">
        <ul class="nav nav-footer justify-content-center justify-content-lg-end">
          <li class="nav-item">
            <a href="https://www.creative-tim.com" class="nav-link" target="_blank">Creative Tim</a>
          </li>
          <li class="nav-item">
            <a href="https://www.creative-tim.com/presentation" class="nav-link" target="_blank">About Us</a>
          </li>
          <li class="nav-item">
            <a href="http://blog.creative-tim.com" class="nav-link" target="_blank">Blog</a>
          </li>
          <li class="nav-item">
            <a href="https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md" class="nav-link" target="_blank">MIT License</a>
          </li>
        </ul>
      </div>
    </div>
  </footer>
</div>