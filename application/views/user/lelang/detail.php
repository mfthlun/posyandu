<!-- Header -->
<div class="header bg-success pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Detail Lelang</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home text-success"></i></a></li>
                  <li class="breadcrumb-item active" aria-current="page">Detail Lelang</li>
                </ol>
              </nav>
            </div>
            <!-- <div class="col-lg-6 col-5 text-right">
              <a href="#" class="btn btn-sm btn-neutral text-success">New</a>
              <a href="#" class="btn btn-sm btn-neutral text-success">Filters</a>
            </div> -->
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row justify-content-center">
        <div class="col">
          <div class="card">
            <!-- <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 class="mb-0">Detail Barang</h3>
                </div>
              </div>
            </div> -->
            <div class="card-body">
              <div class="row">
                <?php
                  $no = 1;
                ?>
                <div class="col-lg-5 col-md-6">
                  <div class="card">
                    <img class="card-img-top card-img-overlay" src="<?php echo base_url() ?>/upload/lelang/<?php echo $data_barang->gambar_barang ?>">
                  </div>
                </div>
                <div class="col-lg-7 col-md-6">
                  <div class="card">
                    <div class="card-body">
                      <h2 class="mb-1"><?php echo $data_barang->nama_barang ?></h2>
                      <h3 class="mb-2"><?php echo $data_barang->harga_barang ?></h3>
                      <p>Jenis : <?php echo $data_barang->jenis_barang ?><br>
                      Pemilik : <?php echo $data_barang->pemilik_barang ?><br>
                      Batas Tebus Barang : <?php echo $data_barang->batas_tebus_barang ?>
                      </p>
                      <div id="timer"></div>
                      <div class="form-inline">
                        <form action="<?php echo base_url('user/tawar_action') ?>" method = "post">
                          <label for="harga_penawaran" class="sr-only">Tawar Harga:</label>
                          <input type="text" id="harga_penawaran" name="harga_penawaran" class="form-control" placeholder="Tawar harga">
                          <input type="hidden" id="id_penawar" name="id_penawar" value = "<?php echo $this->session->userdata('user_id') ?>">
                          <input type="hidden" id="nama_penawar" name="nama_penawar" value = "<?php echo $this->session->userdata('username') ?>">
                          <input type="hidden" id="id_barang_lelang" name="id_barang_lelang" value = "<?php echo $data_barang->id_barang ?>">
                          <input type="hidden" id="status_penawaran" name="status_penawaran" value = "Pending">
                          <input class="btn btn-success text-neutral" type="submit" value="Bid" id = "btn_bid"><br><br>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      <div class="row justify-content-center">
        <div class="col">
          <div class="card">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 class="mb-0">Penawaran Barang</h3>
                </div>
              </div>
            </div>
            
            <div class="card-body">
                <div class="row">
                    <div class="table-responsive">
                        <!-- Tabel -->
                        <table id="table" class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">No.</th>
                                <th scope="col">Nama Penawar</th>
                                <th scope="col">Harga Tawar</th>
                                <th scope="col">Status Penawaran</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                if(!empty($data_tawar)){
                                  foreach($data_tawar as $tawar): ?>
                                <tr>
                                    <td><?php echo $no++ ?></td>
                                    <td><?php echo $tawar->nama_penawar ?></td>
                                    <td><?php echo $tawar->harga_penawaran ?></td>
                                    <td><?php echo $tawar->status_penawaran ?></td>
                                </tr>
                                <?php endforeach;
                                }else{ ?>
                                <tr>
                                    <td colspan="4" class="text-center"><h3>Belum ada yang menawar</h3></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Footer -->
      <footer class="footer pt-0">
        <div class="row align-items-center justify-content-lg-between">
          <div class="col-lg-6">
            <div class="copyright text-center  text-lg-left  text-muted">
              &copy; 2020 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1 text-success" target="_blank">Creative Tim</a>
            </div>
          </div>
          <div class="col-lg-6">
            <ul class="nav nav-footer justify-content-center justify-content-lg-end">
              <li class="nav-item">
                <a href="https://www.creative-tim.com" class="nav-link" target="_blank">Creative Tim</a>
              </li>
              <li class="nav-item">
                <a href="https://www.creative-tim.com/presentation" class="nav-link" target="_blank">About Us</a>
              </li>
              <li class="nav-item">
                <a href="http://blog.creative-tim.com" class="nav-link" target="_blank">Blog</a>
              </li>
              <li class="nav-item">
                <a href="https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md" class="nav-link" target="_blank">MIT License</a>
              </li>
            </ul>
          </div>
        </div>
      </footer>
    </div>
    
    
    