<?php
foreach ($data_user as $data) {
    $user_id = $data->user_id;
    $username = $data->username;
    $nama_lengkap = $data->nama_lengkap;
    $password = $data->password;
    $email = $data->email;
    // $pemilik_barang = $data->pemilik_barang;
    // $nama_barang = $data->nama_barang;
    // $jenis_barang = $data->jenis_barang;
    // $batas_tebus_barang = $data->batas_tebus_barang;
}
?>
<!-- Header -->
<div class="header bg-success pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <!-- <h6 class="h2 text-white d-inline-block mb-0">Barang</h6> -->
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-home text-success"></i></a></li>
              <li class="breadcrumb-item"><a href="#" class="text-success">Dashboards</a></li>
              <li class="breadcrumb-item active" aria-current="page">Users Management</li>
              <li class="breadcrumb-item active" aria-current="page">Ubah Data User</li>
            </ol>
          </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
          <a href="<?php echo base_url('Admin/barang') ?>" class="btn btn-neutral text-success">List Barang</a>
          <!-- <a href="#" class="btn btn-neutral text-success">Filters</a> -->
        </div>
      </div>
      <!-- Card stats -->
      <div class="row">
        <div class="col-xl-3 col-md-6">

        </div>
      </div>
    </div>
  </div>
</div>

<!-- Page content - Tabel data -->

<div class="container-fluid mt--6">
  <div class="row">
    <div class="col-xl-12">
      <div class="card">
        <div class="card-header border-0">
          <div class="row align-items-center">
            <div class="col">
              <h3 class="mb-0">Ubah data user</h3>
            </div>
          </div>
        </div>
        <div class="card-body">
          <form class="form-horizontal" action = "<?= base_url(). 'Admin/user_edit_action'; ?>" method = "post">
          <input class="form-control" type="hidden" id="user_id" name="user_id" value="<?= $this->uri->segment(3); ?>" required>
          <div class="md-form mb-5">
            <label>Username</label>
            <input name="username" id="username" type="text" class="form-control" value="<?= $username; ?>" required>
          </div>
          <div class="md-form mb-5">
            <label>Nama Lengkap</label>
            <input name="nama_lengkap" id="nama_lengkap" type="text" class="form-control" value="<?= $nama_lengkap; ?>" required>
          </div>
          <div class="md-form mb-5">
            <label>Email</label>
            <input name="email" id="email" type="text" class="form-control" value="<?= $email; ?>" required>
          </div>
          <div class="md-form mb-5">
            <label>Password</label>
            <input name="password" id="password" type="password" class="form-control" value="<?= $password; ?>" required>
          </div>
          <div class="modal-footer d-flex justify-content-center">
              <button class="btn btn-primary" id="inputButton">Input</button>
          </div>
          </form>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!-- Footer -->
  <footer class="footer pt-0">
    <div class="row align-items-center justify-content-lg-between">
      <div class="col-lg-6">
        <div class="copyright text-center  text-lg-left  text-muted">
          &copy; 2020 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1 text-success" target="_blank">Creative Tim</a>
        </div>
      </div>
      <div class="col-lg-6">
        <ul class="nav nav-footer justify-content-center justify-content-lg-end">
          <li class="nav-item">
            <a href="https://www.creative-tim.com" class="nav-link" target="_blank">Creative Tim</a>
          </li>
          <li class="nav-item">
            <a href="https://www.creative-tim.com/presentation" class="nav-link" target="_blank">About Us</a>
          </li>
          <li class="nav-item">
            <a href="http://blog.creative-tim.com" class="nav-link" target="_blank">Blog</a>
          </li>
          <li class="nav-item">
            <a href="https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md" class="nav-link" target="_blank">MIT License</a>
          </li>
        </ul>
      </div>
    </div>
  </footer>
</div>