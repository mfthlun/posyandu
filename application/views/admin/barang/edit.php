<!-- Header -->
<div class="header bg-success pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <!-- <h6 class="h2 text-white d-inline-block mb-0">Barang</h6> -->
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-home text-success"></i></a></li>
              <li class="breadcrumb-item"><a href="#" class="text-success">Dashboards</a></li>
              <li class="breadcrumb-item active" aria-current="page">Barang</li>
              <li class="breadcrumb-item active" aria-current="page">Ubah Data Barang</li>
            </ol>
          </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
          <a href="<?php echo base_url('Admin/barang') ?>" class="btn btn-neutral text-success">List Barang</a>
          <!-- <a href="#" class="btn btn-neutral text-success">Filters</a> -->
        </div>
      </div>
      <!-- Card stats -->
      <div class="row">
        <div class="col-xl-3 col-md-6">

        </div>
      </div>
    </div>
  </div>
</div>

<!-- Page content - Tabel data -->

<div class="container-fluid mt--6">
  <div class="row">
    <div class="col-xl-12">
      <div class="card">
        <div class="card-header border-0">
          <div class="row align-items-center">
            <div class="col">
              <h3 class="mb-0">Ubah data barang lelang</h3>
            </div>
          </div>
        </div>
        <div class="card-body">
          <form class="form-horizontal" action = "<?= base_url(). 'Admin/barang_edit_action'; ?>" method = "post" enctype="multipart/form-data">
          <input class="form-control" type="hidden" id="id_barang" name="id_barang" value="<?= $this->uri->segment(3); ?>" required>
          <input class="form-control" type="hidden" id="gambar_barang_old" name="gambar_barang_old" value="<?= $data_barang->gambar_barang; ?>" required>
          <div class="md-form mb-5">
            <label>Nama Barang</label>
            <input name="nama_barang" id="nama_barang" type="text" class="form-control" value="<?= $data_barang->nama_barang; ?>" required>
          </div>
          <div class="md-form mb-5">
            <label>Pemilik Barang</label>
            <input name="pemilik_barang" id="pemilik_barang" type="text" class="form-control" value="<?= $data_barang->pemilik_barang; ?>" required>
          </div>
          <div class="row">
            <div class="col-sm md-form mb-5">
              <label>Harga Barang</label>
              <input name="harga_barang" id="harga_barang" type="number" class="form-control" value="<?= $data_barang->harga_barang; ?>" required>
            </div>
            <div class="col-sm md-form mb-5">
              <label> Kategori Barang</label>
              <select name="jenis_barang" id="jenis_barang" class="form-control" required>
                <option value="" selected disabled>--Silahkan pilih--</option>
                <?php 
                  foreach($kategori as $kat):
                ?>
                <option value="<?php echo $kat->nama_kategori ?>" <?php echo ($data_barang->jenis_barang==$kat->nama_kategori) ? "selected = 'selected'" : "" ;?>> <?php echo $kat->nama_kategori ?> </option>
                <?php endforeach ?>
              </select>
            </div>
          </div>
          <div class="row">
            <div class="col-sm md-form mb-5">
              <label>Gambar</label>
              <input name="gambar_barang" id="gambar_barang" type="file" class="form-control">
              <div class="img-thumbnail">
                <img src="<?= base_url('upload/lelang/').$data_barang->gambar_barang; ?>" width="200px">
              </div>
            </div>
            <div class="col-sm md-form mb-5">
              <label>Batas Tebus Barang</label>
              <input name="batas_tebus_barang" id="batas_tebus_barang" type="date" class="form-control" value="<?= $data_barang->batas_tebus_barang; ?>" required>
            </div>
            <div class="col-sm md-form mb-5">
              <label>Status Lelang</label>
              <select name="status_lelang" id="status_lelang" class="form-control" required>
                <option value="" selected disabled>--Silahkan pilih--</option>
                <option value="0" <?php echo ($data_barang->status=="0") ? "selected = 'selected'" : "" ;?>>Belum Aktif</option>
                <option value="1" <?php echo ($data_barang->status=="1") ? "selected = 'selected'" : "" ;?>>Aktif</option>
                <option value="2" <?php echo ($data_barang->status=="2") ? "selected = 'selected'" : "" ;?>>Selesai</option>
              </select>
            </div>
          </div>
          <div class="modal-footer d-flex justify-content-center">
              <button class="btn btn-primary" id="inputButton">Input</button>
          </div>
          </form>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!-- Footer -->
  <footer class="footer pt-0">
    <div class="row align-items-center justify-content-lg-between">
      <div class="col-lg-6">
        <div class="copyright text-center  text-lg-left  text-muted">
          &copy; 2020 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1 text-success" target="_blank">Creative Tim</a>
        </div>
      </div>
      <div class="col-lg-6">
        <ul class="nav nav-footer justify-content-center justify-content-lg-end">
          <li class="nav-item">
            <a href="https://www.creative-tim.com" class="nav-link" target="_blank">Creative Tim</a>
          </li>
          <li class="nav-item">
            <a href="https://www.creative-tim.com/presentation" class="nav-link" target="_blank">About Us</a>
          </li>
          <li class="nav-item">
            <a href="http://blog.creative-tim.com" class="nav-link" target="_blank">Blog</a>
          </li>
          <li class="nav-item">
            <a href="https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md" class="nav-link" target="_blank">MIT License</a>
          </li>
        </ul>
      </div>
    </div>
  </footer>
</div>