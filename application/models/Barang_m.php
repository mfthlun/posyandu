<?php

class Barang_m extends CI_Model {
  private $_table = "barang";

  public $id_barang;
  public $pemilik_barang;
  public $nama_barang;
  public $jenis_barang;
  public $batas_tebus_barang;
  public $harga_barang;
  // public $image = "default.jpg";

  public function rules()
  {
    return [
      ['field' => 'pemilik_barang',
      'label' => 'Pemilik Barang',
      'rules' => 'required'],

      ['field' => 'nama_barang',
      'label' => 'Nama Barang',
      'rules' => 'required'],

      ['field' => 'jenis_barang',
      'label' => 'Jenis Barang',
      'rules' => 'required'],
      
      ['field' => 'batas_tebus_barang',
      'label' => 'Batas Tebus Barang',
      'rules' => 'required']
    ];
  }

  function SelectAll(){
    return $this->db->get($this->_table)->result();
  }

  function SelectByID($id_barang){
    //untuk menampilkan data lebih dari satu baris, di view harus foreach
    // return $this->db->get_where($this->_table,['id_barang'=>$id_brg])->result();
    // Untuk menampilkan satu data, di view langsung echo
    return $this->db->get_where($this->_table,['id_barang'=>$id_barang])->row();
  }

  function SelectWhere($where){
    // return $this->db->where($where)->get($this->_table)->result();
    return $this->db->get_where($this->_table,$where)->result();
  }

  function SelectStatusAktif(){
    return $this->db->get_where($this->_table,['status'=>1])->result();
  }

  function Insert($data){
    return $this->db->insert($this->_table, $data);
  }

  function Update($data){
    $this->db->update($this->_table, $data, ['id_barang'=>$data['id_barang']]);
  }

  function Delete($data){
    $id_barang = $data['id_barang'];
    $gambar = $this->db->get_where($this->_table,['id_barang'=>$id_barang])->row();
    unlink("upload/lelang/".$gambar->gambar_barang);
    return $this->db->delete($this->_table, ["id_barang"=>$id_barang]);
  }

  function SelectHargaBarangById($id_barang) {
    return $this->db->select("harga_barang")
                ->get_where($this->_table,['id_barang'=>$id_barang])->row();
  }
}

?>