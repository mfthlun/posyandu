<?php

class Kategori_m extends CI_Model {
  private $_table = "kategori";

  public $id_kategori;
  public $nama_kategori;
  public $jumlah_barang;

  public function rules()
  {
    return [
      ['field' => 'id_kategori',
      'label' => 'ID Kategori',
      'rules' => 'required'],

      ['field' => 'nama_kategori',
      'label' => 'Nama Kategori',
      'rules' => 'required'],

      ['field' => 'jumlah_barang',
      'label' => 'Jumlah Barang',
      'rules' => 'required']
    ];
  }

  function SelectAll(){
    return $this->db->get($this->_table)->result();
  }

  //untuk menampilkan data lebih dari satu baris, di view harus foreach
  // return $this->db->get_where($this->_table,['id_barang'=>$id_brg])->result();
  function SelectByID($id_kategori){
    return $this->db->get_where($this->_table,['id_kategori'=>$id_kategori])->row();
  }

  function Insert($data){
    return $this->db->insert($this->_table, $data);
  }

  function Update($data){
    $this->db->update($this->_table, $data, ['id_kategori'=>$data['id_kategori']]);
  }

  function Delete($data){
    $id_kategori = $data['id_kategori'];
    return $this->db->delete($this->_table, ["id_kategori"=>$id_kategori]);
  }
}
?>