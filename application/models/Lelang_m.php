<?php

class Lelang_m extends CI_Model {
  private $_table = "penawaran_lelang";

  public $id_penawar_lelang;
  public $id_barang_lelang;
  public $id_penawar;
  public $nama_penawar;
  public $harga_penawaran;
  public $status_penawaran;  

  // public $image = "default.jpg";

//   public function rules()
//   {
//     return [
//       ['field' => 'pemilik_barang',
//       'label' => 'Pemilik Barang',
//       'rules' => 'required'],

//       ['field' => 'nama_barang',
//       'label' => 'Nama Barang',
//       'rules' => 'numeric'],

//       ['field' => 'jenis_barang',
//       'label' => 'Jenis Barang',
//       'rules' => 'numeric'],
      
//       ['field' => 'batas_tebus_barang',
//       'label' => 'Batas Tebus Barang',
//       'rules' => 'required']
//     ];
//   }

  function SelectAll(){
    return $this->db->get($this->_table)->result();
  }

  // function SelectByID($id_brg){
  //   return $this->db->get_where($this->_table,['id_barang'=>$id_brg])->result();
  //   // Untuk menampilkan satu data aja harusnya row, tapi viewnya/controllernya harus diubah
  //   // return $this->db->get_where($this->_table,['id_barang'=>$id_barang])->row();
  // }

  // function SelectByID($where2){
  //   return $this->db->where($where2)
  //                   ->order_by('id_penawar_lelang', 'desc')
  //                   ->get($this->_table)
  //                   ->result();
  // }

  function Insert($data){
    return $this->db->insert($this->_table, $data);
  }

//   function Update($data){
//     $this->db->update($this->_table, $data, ['id_barang'=>$data['id_barang']]);
//   }

//   function Delete($data){
//     $id_barang = $data['id_barang'];
//     return $this->db->delete($this->_table, ["id_barang"=>$id_barang]);
//   }

  function SelectPenawaranByUser($id_penawar){
    return $this->db->get_where($this->_table,['id_penawar'=>$id_penawar])->result();
  }

  function SelectPenawaran($id_barang){
    return $this->db->order_by('id_penawar_lelang', 'desc')
                    ->get_where($this->_table,['id_barang_lelang'=>$id_barang])
                    ->result();
  }

  function SelectHargaPenawaran($id_barang) {
    return $this->db->select_max("harga_penawaran")
                ->get_where($this->_table,['id_barang_lelang'=>$id_barang])->row();
  }
  
}

?>