<?php

class User_m extends CI_Model {
  private $_table = "user";

  public $user_id;
  public $username;
  public $password;
  public $level;
  public $email;

  function SelectAll(){
    return $this->db->get($this->_table)->result();
  }

  function SelectByID($id_barang){
    return $this->db->get_where($this->_table,['user_id'=>$id_barang])->result();
  }

  function Insert($data){
    return $this->db->insert($this->_table, $data);
  }

  function Update($data){
    $this->db->update($this->_table, $data, ['user_id'=>$data['user_id']]);
  }

  function Delete($data){
    $id_barang = $data['user_id'];
    return $this->db->delete($this->_table, ["user_id"=>$id_barang]);
  }
}

?>